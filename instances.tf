# 
# EC2 1 Amazon Linux 
#
data "aws_ami" "amazon-linux-2" {
  most_recent = true

 # filter {
   # name   = "owner-alias"
  #  values = ["amazon"]
 # }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }

  owners = ["amazon"]
}

resource "aws_instance" "ec2-test" {
  ami           = data.aws_ami.amazon-linux-2.id
  availability_zone = "us-east-2a"
  subnet_id = aws_subnet.juan-vpc1-sn1.id
  instance_type = "t2.micro"
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  key_name = "juan-keypair"

  tags = {
    Name = "juan-ec2-ami"
  }
}
 # VOLUME
 resource "aws_ebs_volume" "juan-ec2-vol" {
  availability_zone = "us-east-2a"
  size              = 15
  type = "gp2"
  
  tags = {
    Name = "juan-ec2-vol"
  }
}
resource "aws_volume_attachment" "juan-vpc1-vol-att" {
  device_name = "/dev/xvdb"
  volume_id   = aws_ebs_volume.juan-ec2-vol.id
  instance_id = aws_instance.ec2-test.id
}