#Before Creation 
data "aws_availability_zones" "available" {}


#
#Create new VPC 1
#

resource "aws_vpc" "juan-vpc1" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "juan-vpc1"
  }
}

resource "aws_subnet" "juan-vpc1-sn1" {
  vpc_id     = aws_vpc.juan-vpc1.id
  cidr_block = "10.0.0.0/24"
  availability_zone = "us-east-2a"

  tags = {
    Name = "juan-vpc1-sn1"
  }
}

resource "aws_subnet" "juan-vpc1-sn2" {
  vpc_id     = aws_vpc.juan-vpc1.id
  cidr_block = "10.0.16.0/24"
  availability_zone = "us-east-2c"

  tags = {
    Name = "juan-vpc1-sn2"
  }
}

#
# Internet gateway
#

resource "aws_internet_gateway" "juan-vpc1-gw" {
  vpc_id = aws_vpc.juan-vpc1.id

  tags = {
    Name = "juan-vpc1-gw"
  }
}

#
# END VPC 1
#

#
# Route table with routes within
#

resource "aws_route_table" "juan-vpc1-rt1" {
  vpc_id = aws_vpc.juan-vpc1.id



  tags = {
    Name = "juan-vpc1-rt1"
  }
}

resource "aws_route_table" "juan-vpc1-rt2" {
  vpc_id = aws_vpc.juan-vpc1.id

  tags = {
    Name = "juan-vpc1-rt2"
  }
}

resource "aws_route" "juan-vpc1-r1" {
  route_table_id            = aws_route_table.juan-vpc1-rt1.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.juan-vpc1-gw.id
}

resource "aws_route_table_association" "juan-vpc1-rt-asoc1" {
  subnet_id      = aws_subnet.juan-vpc1-sn1.id
  route_table_id = aws_route_table.juan-vpc1-rt1.id
}
